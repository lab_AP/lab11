#ifndef __GRAPH_H
#define __GRAPH_H

#define MAX_L_NAME 20
#define MAX_L_NAME_S "20"

typedef enum {white, gray, black} color;
typedef enum {unknown, tree, back, forward, cross} edge_type;

typedef struct graph_t *GRAPH;

extern int GRAPH_ERR;

typedef struct vertex_t {
  char            *name;
  color            color;
  struct vertex_t *pred;
  int              dist;
  int              d_time;
  int              f_time;
  int              n_edges;
  struct edge_t   *edges;
} vertex;

typedef struct edge_t {
  int        weight;
  edge_type  type;
  vertex    *vertex;
} edge;

typedef struct graph_t {
  int     n_vertices;
  vertex *vertices;
} graph;

GRAPH load_graph( const char * );
void destroy_graph( const GRAPH );
void print_graph( const GRAPH, FILE * );
int BFS( GRAPH, char * );
void DFS( GRAPH g );

vertex *search_vertex_name( const GRAPH, const char * );

#endif
