#include <stdio.h>
#include "graph.h"

void printErr( void );

int main( int argc, char **argv) {
  GRAPH g;
  FILE *fp;

  if( argc != 4 ) {
    printf( "USAGE: pgrm infile startingNode outfile\n" );
    return -1;
  }

  fp = fopen( argv[3], "w");
  if(fp==NULL) {
    printf ("ERROR Opening FIle.\n" );
    return -1;
  }

  g = load_graph( argv[1] );
  if( g == NULL ) {
    printErr(); return -1;
  }

  fprintf (stdout, "BFS:\n");
  fprintf (fp, "BFS:\n");
  if( !BFS( g, argv[2] )) {
    printErr(); destroy_graph( g ); return -1;
  }
  print_graph( g, stdout );
  print_graph( g, fp );

  fprintf (stdout, "DFS:\n");
  fprintf (fp, "DFS:\n");
  DFS( g );
  print_graph( g, stdout );
  print_graph( g, fp );

  fclose( fp );

  destroy_graph( g );

  //system ("pause");
  return 0;
}

void printErr( void ) {
  switch( GRAPH_ERR ) {
    case  1: printf( "ERROR: fopen failed\n" ); break;
    case  2: printf( "ERROR: calloc on g failed\n" ); break;
    case  3: printf( "ERROR: reading number of vertex failed\n" ); break;
    case  4: printf( "ERROR: calloc on vertex array failed\n" ); break;
    case  5: printf( "ERROR: reading <name node> <number of vertex> failed\n" ); break;
    case  6: printf( "ERROR: name copy failed\n" ); break;
    case  7: printf( "ERROR: allocation of edges array failed\n" ); break;
    case  8: printf( "ERROR: reading <name vertex> failed\n" ); break;
    case  9: printf( "ERROR: queue creation failed\n" ); break;
    case 10: printf( "ERROR: vertex research by name failed\n" ); break;
    case 11: printf( "ERROR: queuing failed \n" ); break;
  }
  //system ("pause");
  return;
}
