#ifndef __QUEUE_H
#define __QUEUE_H

/* BASE is defined as type of atomic element of the queue */
typedef void *BASE;

typedef struct queue_t *QUEUE;
/* here the declaration of global variables accessible by the client,
   with "extern" attribute */

QUEUE QUEUE_init( void );
int QUEUE_enqueue( QUEUE q, BASE x );
int QUEUE_dequeue( QUEUE q, BASE *P_x );
int QUEUE_head( QUEUE q, BASE *P_x );
int QUEUE_empty( QUEUE q );
void QUEUE_destroy( QUEUE q );

#endif
