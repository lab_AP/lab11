#include <stdlib.h>
#include "queue.h"
   
typedef struct queue_t {
  BASE info;
  struct queue_t *succ;
} queue;

QUEUE QUEUE_init( void ) {
  QUEUE q;
  if(( q = calloc( 1, sizeof( queue ))) != NULL ) {
    q->succ = NULL;
  }
  return q;
}

int QUEUE_enqueue( QUEUE q, BASE x ) {
  QUEUE t;
  if(( t = calloc( 1, sizeof( queue ))) == NULL ) return 0;
  t->info = x;
  if( q->succ == NULL ) {
    q->succ = t;
    q->succ->succ = t;
  }
  else {
    t->succ = q->succ->succ;
    q->succ->succ = t;
    q->succ = t;
  }
  return 1;
}

int QUEUE_dequeue( QUEUE q, BASE *P_x ) {
  QUEUE t;
  if( q->succ == NULL ) return 0;
  if( q->succ != q->succ->succ ) {
    t = q->succ->succ;
    q->succ->succ = t->succ;
    *P_x = t->info;
    free( t );
  }
  else {
    *P_x = q->succ->info;
    free( q->succ );
    q->succ = NULL;
  }
  return 1;
}

int QUEUE_head( QUEUE q, BASE *P_x ) {
  if( q->succ == NULL ) return 0;
  *P_x = q->succ->succ->info;
  return 1;
}

int QUEUE_empty( QUEUE q ) {
  return q->succ == NULL;
}

void QUEUE_destroy( QUEUE q ) {
  QUEUE t;
  if( q->succ != NULL ) {
    while( q->succ != q->succ->succ ) {
      t = q->succ->succ;
      q->succ->succ = t->succ;
      free( t );
    }
    free( q->succ );
  }
  free( q );
  return;
}
