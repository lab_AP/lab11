#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "queue.h"
#include "graph.h"

int GRAPH_ERR = 0;
/*  1: fopen failed */
/*  2: calloc on g failed */
/*  3: reading number of vertex failed */
/*  4: calloc on vertex array failed */
/*  5: reading <name node> <number of vertex> failed*/
/*  6: name copy failed */
/*  7: allocation of edges array failed */
/*  8: reading <name vertex> failed */
/*  9: queue creation failed */
/* 10: vertex research by name failed */
/* 11: queuing failed */

static char *my_strdup( const char * );
static int empty_vertex( const vertex * );
static void DFS_Visit( vertex *, int *, GRAPH );

void DFS( GRAPH g ) {
  vertex *u;
  int i, time = 0;
  for( i = 0; i < g->n_vertices; i++ ) {
    u = &(g->vertices[i]);
    u->color = white;
    u->pred = NULL;
  }
  for( i = 0; i < g->n_vertices; i++ ) {
    u = &(g->vertices[i]);
    if( u->color == white )
      DFS_Visit( u, &time, g );
  }
  return;
}

static void DFS_Visit( vertex *u, int *P_time, GRAPH g ) {
  int i;
  vertex *v;
  u->color = gray;
  u->d_time = ++(*P_time);
  //print_graph( g, stdout );
  for( i = 0; i < u->n_edges; i++ ) {
    v = u->edges[i].vertex;
    switch( v->color ) {
      case white: u->edges[i].type = tree; break;
      case gray:  u->edges[i].type = back; break;
      case black: if( u->d_time < v->d_time ) /* u is grey, so d_time */
                    u->edges[i].type = forward; /* it has been already assigned */
                  else                          /* v is black, so idem */
                    u->edges[i].type = cross;
                  break;
    }
    if( v->color == white ) {
      v->pred = u;
      DFS_Visit( v, P_time, g );
    }
  }
  u->color = black;
  u->f_time = ++(*P_time);
  //print_graph( g, stdout );
  return;
}

int BFS( GRAPH g, char *root ) {
  vertex *u, *v;
  QUEUE q;
  int i;

  if((q = QUEUE_init()) == NULL ) {
    GRAPH_ERR = 9; return 0;
  }

  for( i = 0; i < g->n_vertices; i++ ) {
    u = &(g->vertices[i]);
    u->color = white;
    u->pred = NULL;
    u->dist = -1;
  }
  if(( u = search_vertex_name( g, root )) == NULL ) {
    QUEUE_destroy( q ); GRAPH_ERR = 10; return 0;
  }
  u->color = gray;
  u->dist = 0;
  if( !QUEUE_enqueue( q, u )) {
    QUEUE_destroy( q ); GRAPH_ERR = 11; return 0;
  }
  //print_graph( g, stdout );
  while( !QUEUE_empty( q )) {
    QUEUE_head( q, (void *)(&u) );
    for( i = 0; i < u->n_edges; i++ ) {
      v = u->edges[i].vertex;
      if( v->color == white ) {
        v->color = gray;
        v->dist = u->dist + 1;
        v->pred = u;
        if( !QUEUE_enqueue( q, v )) {
          QUEUE_destroy( q ); GRAPH_ERR = 11; return 0;
        }
      }
    }
    QUEUE_dequeue( q, (void *)(&u) ); /* u remains equal with itself, but has to be extracted */
    u->color = black;
    //print_graph( g, stdout );
  }
  return 1;
}

void print_graph( const GRAPH g, FILE *fp ) {
  vertex *v;
  int i, j;
  if( g != NULL ) {
    for( i = 0; i < g->n_vertices; i++ ) {
      v = &(g->vertices[i]);
      fprintf( fp, "\"%s\" ", v->name == NULL ? "NULL" : v->name );
      switch( v->color ) {
        case white: fprintf( fp, "WHITE " ); break;
        case gray:  fprintf( fp, "GRAY  " ); break;
        case black: fprintf( fp, "BLACK " ); break;
      }
      fprintf( fp, "[^\"%s\"] ", v->pred == NULL ? " " : v->pred->name );
      fprintf( fp, "%3d%3d%3d", v->dist, v->d_time, v->f_time );
      if( v->n_edges > 0 ) {
        fprintf( fp, " -> [" );
        for( j = 0; j < v->n_edges - 1; j++ ) {
          fprintf( fp, "[\"%s\" %d ",
            (v->edges[j].vertex)->name == NULL ? "NULL" :
            (v->edges[j].vertex)->name, v->edges[j].weight );
          switch( v->edges[j].type ) {
            case unknown: fprintf( fp, "     ]" ); break;
            case tree:    fprintf( fp, "TREE ]" ); break;
            case back:    fprintf( fp, "BACK ]" ); break;
            case forward: fprintf( fp, "FORW ]" ); break;
            case cross:   fprintf( fp, "CROSS]" ); break;
            default:      fprintf( fp, "@@@@@]" ); break;
          }
        }
        if( j < v->n_edges ) {
          fprintf( fp, "[\"%s\" %d ",
            (v->edges[j].vertex)->name == NULL ? "NULL" :
            (v->edges[j].vertex)->name, v->edges[j].weight );
          switch( v->edges[j].type ) {
            case tree:    fprintf( fp, "TREE ]" ); break;
            case back:    fprintf( fp, "BACK ]" ); break;
            case forward: fprintf( fp, "FORW ]" ); break;
            case cross:   fprintf( fp, "CROSS]" ); break;
            default:      fprintf( fp, "     ]" ); break;
          }
        }
      }
      fprintf( fp, "]\n" );
    }
    fprintf( fp, "\n" );
  }
  return;
}

GRAPH load_graph( const char *filename ) {
  FILE *fp;
  GRAPH g;
  int i, j, n_vertices, n_edges;
  char name[MAX_L_NAME+1];
  vertex *v, *u;

  if(( fp = fopen( filename, "r")) == NULL ) {
    GRAPH_ERR = 1; return NULL;
  }
  if(( g = calloc( 1, sizeof( graph ))) == NULL ) {
    GRAPH_ERR = 2; return NULL;
  }
  if( fscanf( fp, "%d", &n_vertices ) != 1 ) {
    GRAPH_ERR = 3; free( g ); return NULL;
  }
  g->n_vertices = n_vertices;
  if(( g->vertices = calloc( n_vertices, sizeof( vertex ))) == NULL ) {
    GRAPH_ERR = 4; free( g ); return NULL;
  }
  for( i = 0; i < n_vertices; i++ ) {
    u = &(g->vertices[i]);
    u->name = NULL;
    u->color = white;
    u->pred = NULL;
    u->dist = u->d_time = u->f_time = -1;
    u->n_edges = 0;
    u->edges = NULL;
  }
  for( i = 0; i < n_vertices; i++ ) {
    if( fscanf( fp, "%" MAX_L_NAME_S "s%d", name, &n_edges ) != 2 ) {
      GRAPH_ERR = 5; destroy_graph( g ); return NULL;
    }
    if(( u = search_vertex_name( g, name )) == NULL ) {
      GRAPH_ERR = 10; destroy_graph( g ); return 0;
    }
    if( empty_vertex( u )) {
      if(( u->name = my_strdup( name )) == NULL ) {
        GRAPH_ERR = 6; destroy_graph( g ); return NULL;
      }
    }
    u->n_edges  = n_edges;
    if(( u->edges = calloc( n_edges, sizeof( edge ))) == NULL ) {
      GRAPH_ERR = 7; destroy_graph( g ); return NULL;
    }
    for( j = 0; j < n_edges; j++ ) {
      if( fscanf( fp, "%" MAX_L_NAME_S "s", name ) != 1 ) {
        GRAPH_ERR = 8; destroy_graph( g ); return NULL;
      }
      if(( v = search_vertex_name( g, name )) == NULL ) {
        GRAPH_ERR = 10; destroy_graph( g ); return 0;
      }
      if( empty_vertex( v )) {
        if(( v->name = my_strdup( name )) == NULL ) {
          GRAPH_ERR = 6; destroy_graph( g ); return NULL;
        }
      }
      u->edges[j].vertex = v;
      u->edges[j].weight = 0;
    }
  }
  fclose( fp );
  return g;
}

void destroy_graph( const GRAPH g ) {
  vertex *P_v;
  int i;
  if( g != NULL ) {
    for( i = 0; i < g->n_vertices; i++ ) {
      P_v = &(g->vertices[i]);
      free( P_v->name );
      free( P_v->edges );
    }
    free( g->vertices );
    free( g );
  }
  return;
}

vertex* search_vertex_name( const GRAPH g, const char *name ) {
  int i;
  for( i = 0; i < g->n_vertices; i++ )
    if( g->vertices[i].name == NULL || strcmp( g->vertices[i].name, name ) == 0 )
      return &(g->vertices[i]);
  return NULL;
}

static int empty_vertex( const vertex *v ) {
  return v->name == NULL;
}

static char *my_strdup( const char *s ) {
  char *t;
  t = malloc( sizeof( char ) * ( strlen( s ) + 1 ));
  if( t != NULL )
    strcpy( t, s );
  return t;
}

